unit uCSP2;

interface

const
  STATUS_OK = 0;
  COMMUNICATIONS_ERROR = -1;
  BAD_PARAM = -2;
  SETUP_ERROR = -3;
  INVALID_COMMAND_NUMBE = -4;
  COMMAND_LRC_ERROR = -7;
  RECIEVED_CHARACTER_ERROR = -8;
  GENERAL_ERROR = -9;
  FILE_NOT_FOUND = 2;
  ACCESS_DENIED = 5;

  PARAM_OFF = 0;
  PARAM_ON = 1;

  DETERMINE_SIZE = 0;

  function csp2Init(aPort : LongInt) : LongInt; stdcall; external 'Csp2.dll';
  function csp2Restore : LongInt; stdcall; external 'Csp2.dll';
  function csp2WakeUp : LongInt; stdcall; external 'Csp2.dll';
  function csp2DataAvailable : LongInt; stdcall; external 'Csp2.dll';

  function csp2ReadData : LongInt; stdcall; external 'Csp2.dll';
  function csp2ClearData : LongInt; stdcall; external 'Csp2.dll';
  function csp2PowerDown : LongInt; stdcall; external 'Csp2.dll';
  function csp2GetTime(aBuffer : PAnsiChar) : LongInt; stdcall; external 'Csp2.dll';
  function csp2SetTime(aBuffer : PAnsiChar) : LongInt; stdcall; external 'Csp2.dll';
  function csp2SetDefaults : LongInt; stdcall; external 'Csp2.dll';

  function csp2GetPacket(barcode : PAnsiChar; nr : integer; maxLength : integer) : LongInt; stdcall; external 'Csp2.dll';
  function csp2GetDeviceID (var aID : PAnsiChar; maxLength : LongInt) : LongInt; stdcall; external 'Csp2.dll';
  function csp2GetProtocol : LongInt; stdcall; external 'Csp2.dll';
  function csp2GetSystemStatus : LongInt; stdcall; external 'Csp2.dll';
  function csp2GetSwVersion(var aVersion : PAnsiChar; maxLength : LongInt) : LongInt; stdcall; external 'Csp2.dll';
  function csp2GetASCIIMode : LongInt; stdcall; external 'Csp2.dll';
  function csp2GetRTCMode : LongInt; stdcall; external 'Csp2.dll'

  function csp2TimeStamp2Str(aStamp : PAnsiChar; aValue : PAnsiChar; maxLength : integer) : LongInt; stdcall; external 'Csp2.dll';

implementation

end.
